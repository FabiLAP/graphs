package exA_10_graph;

import java.util.HashSet;
import java.util.Random;
import java.util.Stack;

public class WeightedGraph {
	
	//fields
	String[] verticesNames;
	int [][] adjacencymatrix;
	Random random;

	public WeightedGraph() {
		// TODO Auto-generated constructor stub
	}
	
	/*
	 * constructor for generating a graph with a size of the argument and without any edges
	 */
	public WeightedGraph(int numberOfVertices) {
		verticesNames = new String[numberOfVertices];
		adjacencymatrix =  new int[numberOfVertices][numberOfVertices];
		random = new Random(7); //added a seed to the randomobject - for testing!!
		
		//init names
		for (int i = 0; i < numberOfVertices; i++){
			verticesNames[i]=""+(char)(i+65);
		}
		
		//init zero edges
		for (int i = 0; i < numberOfVertices; i++){
			for (int j = 0; j < numberOfVertices; j++){
				adjacencymatrix[i][j]=Integer.MAX_VALUE;
			}
		}
	}
	
	/*
	 * constructor for a random generated graph
	 */
	public WeightedGraph (int numberOfVertices, int numberOfEdges){		
		
		this(numberOfVertices);
		
		//init the random edges
		int i = 0;
		while (i<=numberOfEdges){
			int randomRow = random.nextInt(numberOfVertices);
			int randomColumn = random.nextInt(numberOfVertices);
		
			//here doesn't exist an edge - create one
			if(adjacencymatrix[randomRow][randomColumn]==Integer.MAX_VALUE
					&& randomRow!=randomColumn){
				adjacencymatrix[randomRow][randomColumn]=random.nextInt(9)+1; //values from 1 to 9
				i++;
			}
			//there was already an edge
			else{
				//do nothing
			}
		}
	}

	/*
	 * setter for the weights of the Adjacencymatrix
	 */
	public void setAdjacencymatrix(int from, int to, int value) {
		this.adjacencymatrix[from][to] = value;
	}

	/*
	 * prints the adjacencymatrix to the console inclusive the verticesnames
	 */
	public void printAdjacencymatrix(){
		System.out.print("  ");
		for (int i = 0; i < verticesNames.length; i++){
			System.out.print(verticesNames[i]+" ");
		}
		System.out.println();
		for (int i = 0; i < adjacencymatrix.length; i++){
			System.out.print(verticesNames[i]+" ");
			for (int j = 0; j < adjacencymatrix.length; j++){
				if(adjacencymatrix[i][j]==Integer.MAX_VALUE){
					System.out.print("- ");
				}
				else{
					System.out.print(adjacencymatrix[i][j]+" ");
				}				
			}
			System.out.println();
		}
	}
	
	/*
	 * This method calculates the cheapest path in a range.
	 * this is a greedy algorithm
	 * Algorithm is from pseudo code of http://www.youtube.com/watch?v=S8y-Sk7u1So
	 */
	public void findCheapestPath(int start, int end){
		boolean[] visitedPoints = new boolean[adjacencymatrix.length];	//defaultvalue is false
		int[] distanceToStartingPoint = new int[adjacencymatrix.length];
		int[] pointBefore = new int[adjacencymatrix.length];
//		System.out.println("#### Start of method");
		
		//setze pointbefore auf unendlich
		for(int i = 0; i < pointBefore.length; i++){
			pointBefore[i]=Integer.MAX_VALUE;
		}
		//setze distances auf unendlich
		for(int i = 0; i < distanceToStartingPoint.length; i++){
			distanceToStartingPoint[i]=Integer.MAX_VALUE;
		}
	
		distanceToStartingPoint[start]=0;
		pointBefore[start]=start;
		
//		System.out.println("init has finished");
//		printArrays(visitedPoints,distanceToStartingPoint,pointBefore);
		
//		System.out.println("HERE COMES THE LOOP");
		//solange noch nicht alle Punkte besucht wurdn
		while(allVisited(visitedPoints)){
//			System.out.println("stepped into the loop!");
			//finde den Punkt mit der geringsten aktuellen distanz zum startknoten
			// + noch nicht besucht
			int actualPointWithShortestDistance = findShortestDistance(distanceToStartingPoint, visitedPoints);
//			System.out.println("actual point is "+actualPointWithShortestDistance);
			//setze den gefunden Punkt als besucht
			visitedPoints[actualPointWithShortestDistance]=true;
//			System.out.println("actual point gets visited true");
			
//			System.out.println("Genereting a hashset of the neighbors of this point");
			HashSet<Integer> neighborsOfActualPoint = getNeighborsOfPoint(actualPointWithShortestDistance);
			//schleife durch alle nachbarn von aktuellen punkt
			
//			System.out.println("LOOP DURCH DIE NACHBARN");
			for(Integer neighbor : neighborsOfActualPoint){
//				System.out.println("current neighbor: "+neighbor);
				//ist der aktuelle nachbar noch unbesucht
				if(visitedPoints[neighbor]==false){
//					System.out.println("This one wasnt visited already");
					
					//Distanz + jeweiliges Kantengewicht zum nachbarn
					int cache = distanceToStartingPoint[actualPointWithShortestDistance] + adjacencymatrix[actualPointWithShortestDistance][neighbor];
//					System.out.println("Distanz + jeweiliges Kantengewicht zum nachbarn = "+cache);
					
//					System.out.println("next step is checking the distances : cache<distanceToStartingPoint[neighbor]");
					
					if(cache<distanceToStartingPoint[neighbor]){
//						System.out.println("chache was smaller: ");
						//aktualisierung der distance
						distanceToStartingPoint[neighbor]=cache;
//						System.out.println("setting - distanceToStartingPoint[neighbor]=cache");
						//aktualisierung des vorgängers
						pointBefore[neighbor]=actualPointWithShortestDistance;
//						System.out.println("setting - pointBefore[neighbor]=actualPointWithShortestDistance");
						
//						printArrays(visitedPoints,distanceToStartingPoint,pointBefore);
					}
				}
			}		
		}	
//		System.out.println("####Done:#####");
//		printArrays(visitedPoints,distanceToStartingPoint,pointBefore);
		
		//auswerten von pointBefore
		System.out.println("##### cheapest way ######");
		System.out.println(getOrderedRoute(start, end, pointBefore));
	}
	
	/*
	 * Wrapper for Random Start- and Endpoint for the Cheapest Way
	 */
	public void findCheapestPathRandom(){
		int from = random.nextInt(adjacencymatrix.length);
		int to = random.nextInt(adjacencymatrix.length);
		this.findCheapestPath(from, to);
	}

	/*
	 * method for interpreting the results of the pathfinding methods
	 */
	private String getOrderedRoute(int start, int end, int[] pointBefore){
		String toReturn = "";
		int i = end;
		
		Stack<Integer> route = new Stack<Integer>();
		
		while (i!=start){
			route.push(i);
			i=pointBefore[i];			
		}
		toReturn+="from "+verticesNames[start]+" ";
		
		while(!route.isEmpty()){
			toReturn+="to "+verticesNames[route.pop()]+" ";
		}
		return toReturn;
	}
	
	/*
	 * method for evaluating whether all ponits are visited
	 */
	private boolean allVisited (boolean[] tocheck){
		for(int i = 0; i < tocheck.length; i++){
			if (tocheck[i]==false){
				return true;
			}
		}
		return false;
	}

	/*
	 * returns this point with the shortest distance and which isnt visited
	 */
	private int findShortestDistance(int[] tocheck, boolean[] visitedPoints){
		int cache = Integer.MAX_VALUE;
		int index = 0;
		for(int i = 0; i < tocheck.length; i++){
			if(tocheck[i]<cache && !visitedPoints[i]){
				cache = tocheck[i];
				index = i;
			}
		}
		return index;
	}
	
	/*
	 * for testing
	 */
	private void printArrays(boolean[] visitedPoints, int[] distanceToStartingPoint, int[] pointBefore){
		System.out.println("visited points:");
		printbooleanArray(visitedPoints);
		System.out.println("distanceToStartingPoint:");
		printintArray(distanceToStartingPoint);
		System.out.println("pointBefore:");
		printintArray(pointBefore);
		System.out.println("## Finished with printing the arrays");
	}
	
	/*
	 * for testing
	 */
	private void printintArray(int[] toPrint){
		for(int i = 0; i < toPrint.length; i++){
			System.out.println(i+": "+toPrint[i]);
		}
	}
	
	/*
	 * for testing
	 */
	private void printbooleanArray(boolean[] toPrint){
		for(int i = 0; i < toPrint.length; i++){
			System.out.println(i+": "+toPrint[i]);
		}
	}
	
	/*
	 * method for returning all available neighbors of a point
	 */
	private HashSet<Integer> getNeighborsOfPoint(int thePoint){
		HashSet<Integer> toReturn = new HashSet<Integer>();
		for(int i = 0; i < adjacencymatrix.length; i++){
			if(adjacencymatrix[thePoint][i]!=Integer.MAX_VALUE){
//				System.out.println(i+" is a neighbor of actualPoint "+thePoint);
				toReturn.add(i);
			}
		}
		return toReturn;
	}
	/*
	 * Method for calculating the shortest path 
	 * works analog to the findCheapest method
	 * here the weights are "set to 1"
	 * removed comments
	 */
	public void findShortestPath(int start, int end){		
		boolean[] visitedPoints = new boolean[adjacencymatrix.length];	//defaultvalue is false
		int[] distanceToStartingPoint = new int[adjacencymatrix.length];
		int[] pointBefore = new int[adjacencymatrix.length];
		
		for(int i = 0; i < pointBefore.length; i++){
			pointBefore[i]=Integer.MAX_VALUE;
		}
		for(int i = 0; i < distanceToStartingPoint.length; i++){
			distanceToStartingPoint[i]=Integer.MAX_VALUE;
		}	
		distanceToStartingPoint[start]=0;
		pointBefore[start]=start;
		while(allVisited(visitedPoints)){
			int actualPointWithShortestDistance = findShortestDistance(distanceToStartingPoint, visitedPoints);
			visitedPoints[actualPointWithShortestDistance]=true;
			HashSet<Integer> neighborsOfActualPoint = getNeighborsOfPoint(actualPointWithShortestDistance);
			for(Integer neighbor : neighborsOfActualPoint){
				if(visitedPoints[neighbor]==false){
					//### DIFFERENCE (+1)
					int cache = distanceToStartingPoint[actualPointWithShortestDistance] + 1;
					if(cache<distanceToStartingPoint[neighbor]){
						distanceToStartingPoint[neighbor]=cache;
						pointBefore[neighbor]=actualPointWithShortestDistance;
					}
				}
			}		
		}		
		System.out.println("##### shortest way ######");
		System.out.println(getOrderedRoute(start, end, pointBefore));
	}
	
	/*
	 * Wrapper for Random Start- and Endpoint for the shortest way
	 */
	public void findShortestPathRandom(){
		int from = random.nextInt(adjacencymatrix.length);
		int to = random.nextInt(adjacencymatrix.length);
		this.findShortestPath(from, to);
	}
}
