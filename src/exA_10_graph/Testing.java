package exA_10_graph;

public class Testing {

	public Testing() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		WeightedGraph randomGraph = new WeightedGraph(5, 12);
//		randomGraph.printAdjacencymatrix();
//		//try 0 nach 1
//		randomGraph.findCheapestPath(0, 1);
//		randomGraph.findShortestPath(0, 1);
		
		
		
		WeightedGraph youtubeGraph = new WeightedGraph(6);
		youtubeGraph.setAdjacencymatrix(0, 1, 4);
		youtubeGraph.setAdjacencymatrix(1, 0, 4);
		youtubeGraph.setAdjacencymatrix(1, 2, 1);
		youtubeGraph.setAdjacencymatrix(2, 1, 1);
		youtubeGraph.setAdjacencymatrix(0, 2, 2);
		youtubeGraph.setAdjacencymatrix(2, 0, 2);
		youtubeGraph.setAdjacencymatrix(0, 4, 4);
		youtubeGraph.setAdjacencymatrix(4, 0, 4);
		youtubeGraph.setAdjacencymatrix(2, 4, 5);
		youtubeGraph.setAdjacencymatrix(4, 2, 5);
		youtubeGraph.setAdjacencymatrix(2, 3, 3);
		youtubeGraph.setAdjacencymatrix(3, 2, 3);
		youtubeGraph.setAdjacencymatrix(4, 5, 1);
		youtubeGraph.setAdjacencymatrix(5, 4, 1);
		youtubeGraph.setAdjacencymatrix(3, 5, 2);
		youtubeGraph.setAdjacencymatrix(5, 3, 2);		
		youtubeGraph.printAdjacencymatrix();		
		youtubeGraph.findCheapestPath(0, 3);
		youtubeGraph.findShortestPath(0, 3);
		youtubeGraph.findCheapestPath(1, 0);
		youtubeGraph.findShortestPath(1, 0);
		
		WeightedGraph graphicGraph = new WeightedGraph(8);
		graphicGraph.setAdjacencymatrix(0, 1, 3);
		graphicGraph.setAdjacencymatrix(1, 2, 2);
		graphicGraph.setAdjacencymatrix(2, 3, 5);
		graphicGraph.setAdjacencymatrix(2, 6, 1);
		graphicGraph.setAdjacencymatrix(3, 4, 7);
		graphicGraph.setAdjacencymatrix(0, 7, 5);
		graphicGraph.setAdjacencymatrix(7, 0, 4);
		graphicGraph.setAdjacencymatrix(7, 6, 5);
		graphicGraph.setAdjacencymatrix(6, 5, 4);
		graphicGraph.setAdjacencymatrix(5, 4, 4);
		graphicGraph.printAdjacencymatrix();
		graphicGraph.findCheapestPath(0, 4);
//		graphicGraph.findCheapestPath(4, 0);
		graphicGraph.findShortestPath(0, 4);
		
//		WeightedGraph randomGraph2 = new WeightedGraph(20, 40);
//		randomGraph2.printAdjacencymatrix();
//		//try 0 nach 1
//		randomGraph2.findCheapestPath(11, 3);
//		randomGraph2.findShortestPath(11, 3);
		
	}

}
